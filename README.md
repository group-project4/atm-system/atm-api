# ATM API

## Instruction
1. Your computer must have python3.6+ installed.
2. Create python's virtual enviroment [Python's Doc](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/)
3. Install below packanges.
5. Config postgresql connection string in file database.py located in var package folder
6. Run file main.py inside src folder.

### Packages used:
pip install Flask
pip install Flask-SQLAlchemy
pip install flask-expects-json
pip install psycopg2-binary
pip install PyJWT
pip install flask-cors