create_phone_topup_schema = {
    'type': 'object',
    'properties': {
        'topup_amount': {
            'type': 'integer',
            'enum': [
                100,
                200,
                500,
                1000,
                2000,
                5000,
                10000
            ]
        },
        'biller_id': {
            'type': 'integer'
        },
        'phone_number': {
            'type': 'string',
            'minLength': 7,
            'maxLength': 15
        },
        'topup_remark': {
            'type': 'string',
            'minLength': 1
        }
    },
    'required': [
        'topup_amount',
        'biller_id',
        'phone_number'
    ]
}

update_phone_topup_schema = {

}