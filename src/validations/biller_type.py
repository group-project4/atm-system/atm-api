create_biller_type_schema = {
    'type': 'object',
    'properties': {
        'biller_type_name': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 100
        }
    },
    'required': ['biller_type_name']
}

update_biller_type_schema = {
    'type': 'object',
    'properties': {
        'biller_type_id': {
            'type': 'integer'
        },
        'biller_type_name': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 100
        }
    },
    'required': ['biller_type_id', 'biller_type_name']
}