create_billing_schema = {
    'type': 'object',
    'properties': {
        'billing_amount': {
            'type': 'integer',
            'minimum': 1
        },
        'billing_account_number': {
            'type': 'string'
        },
        'remark': {
            'type': 'string',
            'minLength': 1
        },
        'biller_id': {
            'type': 'integer'
        }
    },
    'required': [
        'billing_amount',
        'biller_id',
        'billing_account_number'
    ]
}

update_billing_schema = {
    'type': 'object',
    'properties': {
        'billing_id': {
            'type': 'integer'
        },
        'billing_amount': {
            'type': 'integer',
            'minimum': 1
        },
        'remark': {
            'type': 'string',
            'minLength': 1
        },
        'account_id': {
            'type': 'integer',
        },
        'biller_id': {
            'type': 'integer'
        }
    },
    'required': [
        'billing_id',
        'billing_amount',
        'account_id',
        'biller_id'
    ]
}