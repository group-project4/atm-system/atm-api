create_user_schema = {
    'type': 'object',
    'properties': {
        'user_lastname': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 100
        },
        'user_firstname': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 100
        },
        'user_username': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 100
        },
        'user_password': {
            'type': 'string',
            'minLength': 5
        },
        'user_gender': {
            'type': 'string',
            'enum': [
                'male',
                'female'
            ]
        },
        'user_dob': {
            'type': 'string',
            'format': 'date'
        },
        'user_phone': {
            'type': 'string',
            'minLength': 8,
            'maxLength': 30
        },
        'user_email': {
            'type': 'string',
            'format': 'email',
            'maxLength': 100
        },
        'user_address': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 255
        },
        'user_photo': {
            'type': 'string',
            'minLength': 1
        },
        'user_role': {
            'type': 'string',
            'enum': [
                'admin',
                'normal'
            ]
        }
    },
    'required': [
        'user_lastname',
        'user_firstname',
        'user_username',
        'user_password',
        'user_gender',
        'user_dob',
        'user_phone',
        'user_email',
        'user_address',
        'user_photo',
        'user_role'
    ]
}

update_user_schema = {
    'type': 'object',
    'properties': {
        'user_id': {
            'type': 'integer'
        },
        'user_lastname': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 100
        },
        'user_firstname': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 100
        },
        'user_password': {
            'type': 'string',
            'minLength': 10
        },
        'user_gender': {
            'type': 'string',
            'enum': [
                'male',
                'female'
            ]
        },
        'user_dob': {
            'type': 'string',
            'format': 'date'
        },
        'user_phone': {
            'type': 'string',
            'minLength': 8,
            'maxLength': 30
        },
        'user_email': {
            'type': 'string',
            'format': 'email',
            'maxLength': 100
        },
        'user_address': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 255
        },
        'user_photo': {
            'type': 'string',
            'minLength': 1
        }
    },
    'required': [
        'user_lastname',
        'user_firstname',
        'user_password',
        'user_gender',
        'user_dob',
        'user_phone',
        'user_email',
        'user_address',
        'user_photo'
    ]
}

change_password_schema = {
    'type': 'object',
    'properties': {
        'old_password': {
            'type': 'string',
            'minLength': 5
        },
        'new_password': {
            'type': 'string',
            'minLength': 5
        }
    },
    'required': [
        'old_password',
        'new_password'
    ]
}

update_active_status_schema = {
    'type': 'object',
    'properties': {
        'is_active': {
            'type': 'boolean'
        },
        'user_id': {
            'type': 'integer'
        }
    },
    'required': [
        'is_active',
        'user_id'
    ]
}