create_transfer_schema = {
    'type': 'object',
    'properties': {
        'transfer_to_account': {
            'type': 'integer'
        },
        'transfer_amount': {
            'type': 'integer'
        },
        'transfer_remark': {
            'type': 'string',
            'minLength': 1
        }
    },
    'required': [
        'transfer_to_account',
        'transfer_amount'
    ]
}

update_transfer_schema = {

}