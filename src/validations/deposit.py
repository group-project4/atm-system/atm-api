create_deposit_schema = {
    'type': 'object',
    'properties': {
        'deposit_amount': {
            'type': 'integer'
        }
    },
    'required': [
        'deposit_amount',
    ]
}

update_deposit_schema = {

}