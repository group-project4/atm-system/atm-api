create_biller_schema = {
    'type': 'object',
    'properties': {
        'biller_name': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 100
        },
        'biller_location': {
            'type': 'string',
        },
        'biller_phone': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 30,
        },
        'biller_email': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 255
        },
        'biller_remark': {
            'type': 'string',
            'minLength': 1
        },
        'biller_logo': {
            'type': 'string'
        },
        'biller_type_id': {
            'type': 'integer'
        }
    },
    'required': [
        'biller_name',
        'biller_location',
        'biller_phone',
        'biller_email',
        'biller_logo',
        'biller_type_id'
    ]
}

update_biller_schema = {
    'type': 'object',
    'properties': {
        'biller_id': {
            'type': 'integer'
        },
        'biller_name': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 100
        },
        'biller_location': {
            'type': 'string',
        },
        'biller_phone': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 30,
        },
        'biller_email': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 255
        },
        'biller_remark': {
            'type': 'string',
            'minLength': 1
        },
        'biller_logo': {
            'type': 'string'
        },
        'biller_type_id': {
            'type': 'integer'
        }
    },
    'required': [
        'biller_id',
        'biller_name',
        'biller_location',
        'biller_phone',
        'biller_email',
        'biller_logo',
        'biller_type_id'
    ]
}