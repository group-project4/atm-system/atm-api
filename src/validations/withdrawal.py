create_withdraw_schema = {
    'type': 'object',
    'properties': {
        'withdraw_amount': {
            'type': 'integer'
        }
    },
    'required': [
        'withdraw_amount'
    ]
}

update_withdraw_schema = {

}