from flask import Blueprint, request, jsonify
from database.db import db
from models.biller_type import BillerType
from validations.biller_type import create_biller_type_schema, update_biller_type_schema
from flask_expects_json import expects_json, g
from var import api

biller_type_router = Blueprint(name='biller_type_route', import_name=__name__, url_prefix=api.api_prefix)

@biller_type_router.route('/biller-type', methods=['GET'])
def get_biller():
    '''
    Get all biller types. You can specify an id to get only one biller type.
    '''
    try:
        all_biller_type = BillerType.query.all()

        response_success = {
            'status': api.code_success,
            'data': [biller_type.as_json() for biller_type in all_biller_type]
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@biller_type_router.route('/biller-type/create', methods=['POST'])
@expects_json(schema=create_biller_type_schema)
def create_biller_type():
    try:
        biller_type_name = g.data['biller_type_name']

        billerType = BillerType(biller_type_name=biller_type_name)

        db.session.add(billerType)
        db.session.commit()

        response_success = {
            'status': api.code_success
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 500, 'server error'

@biller_type_router.route('/biller-type/update', methods=['PUT'])
@expects_json(schema=update_biller_type_schema)
def update_biller_type():
    try:
        biller_type_id = g.data['biller_type_id']
        biller_type_name = g.data['biller_type_name']
        
        biller_type = BillerType.query.filter_by(biller_type_id=biller_type_id).first()

        # check if biller type exist or not
        if biller_type is None:
            response_fail = {
                'status': api.code_fail,
                'message': 'Biller type does not exist'
            }
            return jsonify(response_fail), 409

        biller_type.biller_type_name = biller_type_name
        db.session.commit()
        
        response_success = {
            'status': api.code_success,
        }
        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@biller_type_router.route('/biller-type/delete', methods=['DELETE'])
def delete_biller_type():
    return '', 501
