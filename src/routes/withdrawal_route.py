from flask import Blueprint, request, jsonify, g as flask_global
from database.db import db
from models.withdrawal import Withdrawal
from models.account import Account
from validations.withdrawal import create_withdraw_schema, update_withdraw_schema  
from flask_expects_json import expects_json, g
from var import api

withdraw_router = Blueprint(name='withdraw_route', import_name=__name__, url_prefix=api.api_prefix)

@withdraw_router.route('/withdraw', methods=['GET'])
def get_withdraw():
    '''
    Get all withdraw transaction. You can specify an id to get only one withdraw transaction.
    '''
    try:
        all_withdraw = Withdrawal.query.all()

        response_success = {
            'status': api.code_success,
            'data': [withdraw.as_json() for withdraw in all_withdraw]
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@withdraw_router.route('/withdraw/create', methods=['POST'])
@expects_json(schema=create_withdraw_schema)
def create_withdraw():
    try:
        withdraw_amount = g.data['withdraw_amount']
        account_id = flask_global.user['account_id']

        #get account
        account = Account.query.filter_by(account_id=account_id).first()

        if account is None:
            response_fail = {
                'status': api.code_fail,
                'message': f'Account ID={account_id} does not exist'
            }
            return jsonify(response_fail), 409

        # check balance
        if account.balance < withdraw_amount:
            response_fail = {
                'status': api.code_not_enough_balance,
                'message': 'Account does not have enough balance.'
            }
            return jsonify(response_fail), 409

        # deduct account balance
        account.balance = account.balance - withdraw_amount

        withdraw_transaction = Withdrawal(
            withdraw_amount=withdraw_amount,
            account_id=account_id
        )
        
        db.session.add(withdraw_transaction)

        db.session.commit()

        response_success = {
            'status': api.code_success,
            'message': f'You have withdrawed USD{withdraw_amount}',
            'data': {
                'withdraw_date': withdraw_transaction.withdraw_date
            }
        }

        return jsonify(response_success), 200
    except Exception as error:
        db.session.rollback()
        print(error)
        return 'server error', 500

@withdraw_router.route('/withdraw/update', methods=['PUT'])
@expects_json(schema=update_withdraw_schema)
def update_withdraw():
    return '', 501

@withdraw_router.route('/withdraw/delete', methods=['DELETE'])
def delete_withdraw():
    return '', 501
