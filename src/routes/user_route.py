from flask import Blueprint, request, jsonify, g as flask_global
from database.db import db
from models.user import User
from models.account import Account
from validations.user import create_user_schema, update_user_schema, change_password_schema, update_active_status_schema
from flask_expects_json import expects_json, g
from var import api

user_router = Blueprint(name='user_route', import_name=__name__, url_prefix=api.api_prefix)

@user_router.route('/user', methods=['GET'])
def get_users():
    '''
    Get all users. You can specify an id to get only one user.
    '''
    try:
        user_id = request.args.get('user_id')

        all_users = []

        if user_id is not None:
            all_users = User.query.filter_by(user_id=user_id).all()
        else:
            all_users = User.query.all()

        response_success = {
            'status': api.code_success,
            'data': [user.as_json() for user in all_users]
        }
        
        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@user_router.route('/user/current', methods=['GET'])
def get_current_user():
    '''
    Get current user base on token
    '''
    try:
        user_id = flask_global.user.get('user_id')

        current_user = User.query.filter_by(user_id=user_id).first()

        response_success = {
            'status': api.code_success,
            'data': current_user.as_json()
        }
        
        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@user_router.route('/user/account', methods=['GET'])
def get_balance_account():
    '''
    Get account owned by user
    '''
    try:
        account_id = request.args.get('account_id')

        if account_id is None or account_id == '':
            response_fail = {
                'status': api.code_fail,
                'message': 'param account_id is required'
            }
            return jsonify(response_fail), 400

        account = Account.query.filter_by(account_id=account_id).first()

        if account is None:
            response_success = {
                'status': api.code_success,
                'data': None
            }
            return jsonify(response_success), 200

        response_success = {
            'status': api.code_success,
            'data': account.as_json()
        }
        
        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@user_router.route('/user/password/change', methods=['POST'])
@expects_json(schema=change_password_schema)
def change_password():
    try:
        old_password = g.data['old_password']
        new_password = g.data['new_password']
        user_id = flask_global.user['user_id']

        user = User.query.filter_by(user_id=user_id).first()

        if user is None:
            response_fail = {
                'status': api.code_fail,
                'message': 'User does not exist'
            }
            return jsonify(response_fail), 409

        # check user old password match
        if old_password != user.user_password:
            response_fail = {
                'status': api.code_fail,
                'message': 'Old password is incorrect.'
            }
            return jsonify(response_fail), 409

        # update password
        user.user_password = new_password
        db.session.commit()

        response_success = {
            'status': api.code_success,
            'message': 'Password changed'
        }

        return jsonify(response_success), 200

    except Exception as ex:
        print(ex)
        return 'server error', 500

@user_router.route('/user/create', methods=['POST'])
@expects_json(schema=create_user_schema)
def create_user():
    try:
        user_firstname = g.data['user_firstname']
        user_lastname = g.data['user_lastname']
        user_gender = g.data['user_gender']
        user_username = g.data['user_username']
        user_password = g.data['user_password']
        user_dob = g.data['user_dob']
        user_phone = g.data['user_phone']
        user_email = g.data['user_email']
        user_address = g.data['user_address']
        user_photo = g.data['user_photo']
        user_role = g.data['user_role']

        #check username is already taken or not.
        existing_user = User.query.filter(User.user_username.ilike(user_username)).first()

        if existing_user is not None:
            response_fail = {
                'status': api.code_fail,
                'message': f'Username {user_username} is already taken'
            }
            return jsonify(response_fail), 409

         # create account for biller
        account_name = f'{user_lastname} {user_firstname}'
        account = Account(account_name=account_name, account_type='customer')
        db.session.add(account)
        db.session.flush()

        user = User(
            user_firstname=user_firstname,
            user_lastname=user_lastname,
            user_gender=user_gender,
            user_username=user_username,
            user_password=user_password,
            user_dob=user_dob,
            user_phone=user_phone,
            user_email=user_email,
            user_address=user_address,
            user_photo=user_photo,
            user_role=user_role,
            account_id=account.account_id
        )

        db.session.add(user)
        db.session.commit()

        response_success = {
            'status': api.code_success
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@user_router.route('/user/status/update', methods=['PUT'])
@expects_json(schema=update_active_status_schema)
def update_user_status():
    try:
        is_active = g.data['is_active']
        user_id = g.data['user_id']

        # check username is exist or not
        existing_user = User.query.filter_by(user_id=user_id).first()

        if existing_user is None:
            response_fail = {
                'status': api.code_fail,
                'message': f'User does not exist'
            }
            return jsonify(response_fail), 409

        # update user status

        existing_user.is_active = is_active

        db.session.commit()

        response_success = {
            'status': api.code_success
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@user_router.route('/user/update', methods=['PUT'])
@expects_json(schema=update_user_schema)
def update_biller():
    try:
        user_id = g.data['user_id']
        user_firstname = g.data['user_firstname']
        user_lastname = g.data['user_lastname']
        user_password = g.data['user_password']
        user_gender = g.data['user_gender']
        user_dob = g.data['user_dob']
        user_phone = g.data['user_phone']
        user_email = g.data['user_email']
        user_address = g.data['user_address']
        user_photo = g.data['user_photo']

        #get user for update
        user = User.query.filter_by(user_id=user_id).first()

        if user is None:
            response_fail = {
                'status': api.code_fail,
                'message': f'User ID {user_id} does not exist'
            }
            return jsonify(response_fail), 409

        user.user_firstname=user_firstname,
        user.user_lastname=user_lastname,
        user.user_gender=user_gender,
        user.user_password=user_password,
        user.user_dob=user_dob,
        user.user_phone=user_phone,
        user.user_email=user_email,
        user.user_address=user_address,
        user.user_photo=user_photo

        db.session.commit()

        response_success = {
            'status': api.code_success
        }
        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@user_router.route('/user/delete', methods=['DELETE'])
def delete_biller():
    return '', 501
