from flask import Blueprint, request, jsonify
from database.db import db
from models.biller import Biller
from models.biller_type import BillerType
from models.account import Account
from validations.biller import create_biller_schema, update_biller_schema  
from flask_expects_json import expects_json, g
from var import api

biller_router = Blueprint(name='biller_route', import_name=__name__, url_prefix=api.api_prefix)

@biller_router.route('/biller', methods=['GET'])
def get_biller():
    '''
    Get all biller. You can specify an id to get only one biller type.
    '''
    try:
        all_biller = Biller.query.all()

        response_success = {
            'status': api.code_success,
            'data': [biller.as_json() for biller in all_biller]
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@biller_router.route('/biller/biller-type/<biller_type_id>', methods=['GET'])
def get_biller_by_biller_type(biller_type_id):
    '''
    Get billers base on biller type id
    '''

    if biller_type_id is None or biller_type_id == '':
        response_fail = {
            'status': api.code_fail,
            'message': 'Param biller type id is required'
        }
        return jsonify(response_fail), 400

    try:
        biller_type = BillerType.query.filter_by(biller_type_id=biller_type_id).first()

        if biller_type is None:
            response_success = {
                'status': api.code_success,
                'data': None
            }
            return jsonify(response_success), 200

        response_success = {
            'status': api.code_success,
            'data': [biller.as_json() for biller in biller_type.billers]
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@biller_router.route('/biller/biller-type/name/<biller_type_name>', methods=['GET'])
def get_biller_by_biller_type_name(biller_type_name):
    '''
    Get billers base on biller type name
    '''

    if biller_type_name is None or biller_type_name == '':
        response_fail = {
            'status': api.code_fail,
            'message': 'Param biller type name is required'
        }
        return jsonify(response_fail), 400

    try:
        biller_type = BillerType.query.filter(BillerType.biller_type_name.ilike(biller_type_name)).first()

        if biller_type is None:
            response_success = {
                'status': api.code_success,
                'data': None
            }
            return jsonify(response_success), 200

        response_success = {
            'status': api.code_success,
            'data': [biller.as_json() for biller in biller_type.billers]
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@biller_router.route('/biller/create', methods=['POST'])
@expects_json(schema=create_biller_schema)
def create_biller_type():
    try:
        biller_name = g.data['biller_name']
        biller_location = g.data['biller_location']
        biller_phone = g.data['biller_phone']
        biller_email = g.data['biller_email']
        biller_remark = g.data.get('biller_remark')
        biller_logo = g.data['biller_logo']
        biller_type_id = g.data['biller_type_id']

        # check biller type exist or not
        biller_type = BillerType.query.filter_by(biller_type_id=biller_type_id).first()
        if biller_type is None:
            response_fail = {
                'status': api.code_fail,
                'message': f'Biller type ID {biller_type_id} does not exist'
            }
            return jsonify(response_fail), 409

        # create account for biller
        account = Account(account_name=biller_name, account_type='biller')
        db.session.add(account)
        db.session.flush()

        # insert biller to database
        biller = Biller(
            biller_name=biller_name,
            biller_location=biller_location,
            biller_phone=biller_phone,
            biller_email=biller_email,
            biller_remark=biller_remark,
            biller_logo=biller_logo,
            biller_type_id=biller_type_id,
            biller_account_id=account.account_id
        )

        db.session.add(biller)
        db.session.commit()

        response_success = {
            'status': api.code_success
        }

        return jsonify(response_success), 200
    except Exception as error:
        db.session.rollback()
        print(error)
        return 'server error', 500

@biller_router.route('/biller/update', methods=['PUT'])
@expects_json(schema=update_biller_schema)
def update_biller():
    try:
        biller_id = g.data['biller_id']
        biller_name = g.data['biller_name']
        biller_location = g.data['biller_location']
        biller_phone = g.data['biller_phone']
        biller_email = g.data['biller_email']
        biller_remark = g.data.get('biller_remark')
        biller_logo = g.data['biller_logo']
        biller_type_id = g.data['biller_type_id']
        
        # check biller type exist or not
        biller_type = BillerType.query.filter_by(biller_type_id=biller_type_id).first()
        if biller_type is None:
            response_fail = {
                'status': api.code_fail,
                'message': f'Biller type ID {biller_type_id} does not exist'
            }
            return jsonify(response_fail), 409

        biller = Biller.query.filter_by(biller_id=biller_id).first()

        # check if biller exist or not
        if biller is None:
            response_fail = {
                'status': api.code_fail,
                'message': f'Biller ID {biller_id} does not exist'
            }
            return jsonify(response_fail), 409

        # updating data
        biller.biller_name=biller_name
        biller.biller_location=biller_location
        biller.biller_phone=biller_phone
        biller.biller_email=biller_email
        biller.biller_remark=biller_remark
        biller.biller_logo=biller_logo
        biller.biller_type_id=biller_type_id

        db.session.commit()
        
        response_success = {
            'status': api.code_success,
        }
        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@biller_router.route('/biller/delete', methods=['DELETE'])
def delete_biller():
    return '', 501
