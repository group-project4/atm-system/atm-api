from flask import Blueprint, request, jsonify, g as flask_global
from database.db import db
from models.phone_topup import PhoneTopup
from models.account import Account
from models.biller import Biller
from business.currency import convert_cent_to_dollar
from validations.phone_topup import create_phone_topup_schema, update_phone_topup_schema  
from flask_expects_json import expects_json, g
from var import api

phone_topup_router = Blueprint(name='phone_topup_route', import_name=__name__, url_prefix=api.api_prefix)

@phone_topup_router.route('/phone-topup', methods=['GET'])
def get_phone_topup():
    '''
    Get all phone_topup. You can specify an id to get only one phone_topup.
    '''
    try:
        all_phone_topup = PhoneTopup.query.all()

        response_success = {
            'status': api.code_success,
            'data': [phone_topup.as_json() for phone_topup in all_phone_topup]
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@phone_topup_router.route('/phone-topup/create', methods=['POST'])
@expects_json(schema=create_phone_topup_schema)
def create_phone_topup():
    try:
        topup_amount = g.data['topup_amount']
        customer_account_id = flask_global.user['account_id']
        biller_id = g.data['biller_id']
        phone_number = g.data['phone_number']
        remark = g.data.get('topup_remark')

        #get customer account
        customer_account = Account.query.filter_by(account_id=customer_account_id).first()

        if customer_account is None:
            response_fail = {
                'status': api.code_fail,
                'message': f'Account ID={customer_account_id} does not exist'
            }
            return jsonify(response_fail), 409

        # check customer account is disable or not
        if customer_account.is_active is False:
            response_fail = {
                'status': api.code_account_disable,
                'message': 'Your account has been disabled, please contact admin for more information'
            }
            return jsonify(response_fail), 409

        # make sure customer balance is enough to do phone_topup process
        if customer_account.balance < topup_amount:
            response_fail = {
                'status': api.code_not_enough_balance,
                'message': 'Your account does not have enough balance'
            }
            return jsonify(response_fail), 409

        biller = Biller.query.filter_by(biller_id=biller_id).first()

        if biller is None:
            response_fail = {
                'status': api.code_fail,
                'message': f'Biller ID={biller_id} does not exist'
            }
            return jsonify(response_fail), 409

        if biller.account.account_id == customer_account_id:
            response_fail = {
                'status': api.code_fail,
                'message': f'Account ID is not correct'
            }
            return jsonify(response_fail), 409
        
        # deduct phone_topup amount from customer account
        customer_account.balance = customer_account.balance - topup_amount

        # expand biller balance
        biller.account.balance = biller.account.balance + topup_amount

        phone_topup_transaction = PhoneTopup(
            topup_amount=topup_amount, 
            account_id=customer_account_id, 
            biller_id=biller_id, 
            topup_remark=remark,
            phone_number=phone_number
        )

        db.session.add(phone_topup_transaction)
        db.session.flush()
        
        #update last transaction
        customer_account.last_transaction_date = phone_topup_transaction.topup_date 

        db.session.commit()

        response_success = {
            'status': api.code_success,
            'message': f'You have topped up USD{convert_cent_to_dollar(topup_amount)} to {phone_number}',
            'data': {
                'topup_date': phone_topup_transaction.topup_date,
                'type': biller.biller_name
            }
        }

        return jsonify(response_success), 200
    except Exception as error:
        db.session.rollback()
        print(error)
        return 'server error', 500

@phone_topup_router.route('/phone-topup/update', methods=['PUT'])
@expects_json(schema=update_phone_topup_schema)
def update_phone_topup():
    return '', 501

@phone_topup_router.route('/phone-topup/delete', methods=['DELETE'])
def delete_phone_topup():
    return '', 501
