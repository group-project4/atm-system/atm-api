from flask import Blueprint, request, jsonify
from database.db import db
from models.user import User
from models.biller_type import BillerType
from models.account import Account
from validations.auth import login_schema 
from flask_expects_json import expects_json, g
from var import api
from sqlalchemy import and_
from services.jwt_service import generate_token
from datetime import datetime

auth_router = Blueprint(name='auth_route', import_name=__name__, url_prefix=api.api_prefix)

@auth_router.route('/auth/login', methods=['POST'])
@expects_json(schema=login_schema)
def login():
    try:
        username = g.data['username']
        password = g.data['password']

        user = User.query.filter(and_(User.user_username.ilike(username), User.user_password == password)).first()
        
        if user is None:
            response_fail = {
                'status': api.code_fail,
                'message': 'Incorrect username or password'
            }
            return jsonify(response_fail), 409
        
        payload = {
            'user_id': user.user_id,
            'username': user.user_username,
            'user_role': user.user_role,
            'login_date': str(datetime.today()),
            'is_active': user.is_active
        }

        if user.account is not None:
            payload['account_id'] = user.account.account_id

        token = generate_token(payload=payload)

        response_success = {
            'status': api.code_success,
            'message': 'login success',
            'data': {
                'token': token.decode('utf-8'),
                'username': payload['username'],
                'role': payload['user_role'],
                'is_active': payload['is_active']
            }
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

