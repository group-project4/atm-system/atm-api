from flask import Blueprint, request, jsonify, g as flask_global
from database.db import db
from models.deposit import Deposit
from models.account import Account
from validations.deposit import create_deposit_schema, update_deposit_schema  
from flask_expects_json import expects_json, g
from var import api

deposit_router = Blueprint(name='deposit_route', import_name=__name__, url_prefix=api.api_prefix)

@deposit_router.route('/deposit', methods=['GET'])
def get_deposit():
    '''
    Get all deposit transaction. You can specify an id to get only one deposit transaction.
    '''
    try:
        all_deposit = Deposit.query.all()

        response_success = {
            'status': api.code_success,
            'data': [deposit.as_json() for deposit in all_deposit]
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@deposit_router.route('/deposit/create', methods=['POST'])
@expects_json(schema=create_deposit_schema)
def create_deposit():
    try:
        deposit_amount = g.data['deposit_amount']
        account_id = flask_global.user['account_id']

        #get account
        account = Account.query.filter_by(account_id=account_id).first()

        if account is None:
            response_fail = {
                'status': api.code_fail,
                'message': f'Account ID={account_id} does not exist'
            }
            return jsonify(response_fail), 409

        # expand account balance
        account.balance = account.balance + deposit_amount

        deposit_transaction = Deposit(
            deposit_amount=deposit_amount,
            account_id=account_id
        )
        
        db.session.add(deposit_transaction)

        db.session.commit()

        response_success = {
            'status': api.code_success,
            'message': f'You have deposited USD{deposit_amount}',
            'data': {
                'deposit_date': deposit_transaction.deposit_date
            }
        }

        return jsonify(response_success), 200
    except Exception as error:
        db.session.rollback()
        print(error)
        return 'server error', 500

@deposit_router.route('/deposit/update', methods=['PUT'])
@expects_json(schema=update_deposit_schema)
def update_deposit():
    return '', 501

@deposit_router.route('/deposit/delete', methods=['DELETE'])
def delete_deposit():
    return '', 501
