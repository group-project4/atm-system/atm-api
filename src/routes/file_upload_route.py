from flask import Flask, request, Blueprint, jsonify
from var import api
from werkzeug.utils import secure_filename
from os.path import join, abspath
from datetime import datetime

UPLOAD_FOLDER = abspath(join(__file__, '..', '..', 'static'))

file_upload_route = Blueprint(name='file_upload', import_name=__name__, url_prefix=api.api_prefix)

@file_upload_route.route('/file/upload', methods=['POST'])
def upload_file():
    try:
        if 'file' not in request.files:
            response_fail = {
                'status': api.code_fail,
                'message': 'no file found.'
            }
            return jsonify(response_fail), 400

        file = request.files['file']
        now = str(datetime.now().timestamp())
        new_file_name = secure_filename(now + file.filename)
        file.save(join(UPLOAD_FOLDER, new_file_name))

        response_success = {
            'status': api.code_success,
            'message': 'file uploaded',
            'data': {
                'file_name': new_file_name
            }
        }
        
        return jsonify(response_success), 200
    except Exception as ex:
        print(ex)
        return 'server error', 500
