from flask import Blueprint, request, jsonify, g as flask_global
from database.db import db
from models.transfer import Transfer
from models.account import Account
from validations.transfer import create_transfer_schema, update_transfer_schema  
from flask_expects_json import expects_json, g
from var import api

transfer_router = Blueprint(name='transfer_route', import_name=__name__, url_prefix=api.api_prefix)

@transfer_router.route('/transfer', methods=['GET'])
def get_transfer():
    '''
    Get all transfer transaction. You can specify an id to get only one transfer transaction.
    '''
    try:
        all_transfer = Transfer.query.all()

        response_success = {
            'status': api.code_success,
            'data': [transfer.as_json() for transfer in all_transfer]
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@transfer_router.route('/transfer/create', methods=['POST'])
@expects_json(schema=create_transfer_schema)
def create_transfer():
    try:
        transfer_amount = g.data['transfer_amount']
        transfer_sender_id = flask_global.user['account_id']
        transfer_reciever_id = g.data['transfer_to_account']
        remark = g.data.get('transfer_remark')

        if transfer_sender_id == transfer_reciever_id:
            response_fail = {
                'status': api.code_fail,
                'message': 'You can not transfer to your own account'
            }
            return jsonify(response_fail), 409

        #get sender account
        sender_account = Account.query.filter_by(account_id=transfer_sender_id).first()

        if sender_account is None:
            response_fail = {
                'status': api.code_fail,
                'message': f'Sender Account ID={transfer_sender_id} does not exist'
            }
            return jsonify(response_fail), 409

        #check sender account is active or not
        if sender_account.is_active is False:
            response_fail = {
                'status': api.code_account_disable,
                'message': 'Sender account has been disabled.'
            }
            return jsonify(response_fail), 409

        #get reciever account
        reciever_account = Account.query.filter_by(account_id=transfer_reciever_id).first()

        if reciever_account is None:
            response_fail = {
                'status': api.code_fail,
                'message': f'Reciever Account ID={transfer_reciever_id} does not exist'
            }
            return jsonify(response_fail), 409

        # make sure sender account has enough balance
        if (sender_account.balance < transfer_amount):
            response_fail = {
                'status': api.code_not_enough_balance,
                'message': 'Your account does not have enough balance to transfer'
            }
            return jsonify(response_fail), 409

        # deduct balance from sender account
        sender_account.balance = sender_account.balance - transfer_amount
        # expand balance of reciever account
        reciever_account.balance = reciever_account.balance + transfer_amount

        transfer_transaction = Transfer(
            transfer_from_account=transfer_sender_id,
            transfer_to_account=transfer_reciever_id,
            transfer_amount=transfer_amount,
            transfer_remark=remark
        )
        
        db.session.add(transfer_transaction)
        db.session.flush()

        # update last transaction date
        sender_account.last_transaction_date = transfer_transaction.transfer_date

        db.session.commit()

        response_success = {
            'status': api.code_success,
            'message': f'You have transfered USD{transfer_amount} to account {reciever_account.account_name}#{reciever_account.account_id}'
        }

        return jsonify(response_success), 200
    except Exception as error:
        db.session.rollback()
        print(error)
        return 'server error', 500

@transfer_router.route('/transfer/update', methods=['PUT'])
@expects_json(schema=update_transfer_schema)
def update_transfer():
    return '', 501

@transfer_router.route('/transfer/delete', methods=['DELETE'])
def delete_transfer():
    return '', 501
