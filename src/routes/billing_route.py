from flask import Blueprint, request, jsonify, g as flask_global
from database.db import db
from models.billing import Billing
from models.account import Account
from models.biller import Biller
from validations.billing import create_billing_schema, update_billing_schema  
from flask_expects_json import expects_json, g
from var import api

billing_router = Blueprint(name='billing_route', import_name=__name__, url_prefix=api.api_prefix)

@billing_router.route('/billing', methods=['GET'])
def get_billing():
    '''
    Get all billing. You can specify an id to get only one billing.
    '''
    try:
        all_billing = Billing.query.all()

        response_success = {
            'status': api.code_success,
            'data': [billing.as_json() for billing in all_billing]
        }

        return jsonify(response_success), 200
    except Exception as error:
        print(error)
        return 'server error', 500

@billing_router.route('/billing/create', methods=['POST'])
@expects_json(schema=create_billing_schema)
def create_billing():
    try:
        billing_account_number = g.data['billing_account_number']
        billing_amount = g.data['billing_amount']
        customer_account_id = flask_global.user['account_id']
        biller_id = g.data['biller_id']
        remark = g.data.get('remark')

        #get customer account
        customer_account = Account.query.filter_by(account_id=customer_account_id).first()

        # check customer account is disable or not
        if customer_account.is_active is False:
            response_fail = {
                'status': api.code_account_disable,
                'message': 'Your account has been disabled, please contact admin for more information'
            }
            return jsonify(response_fail), 409

        # make sure customer balance is enough to do billing process
        if customer_account.balance < billing_amount:
            response_fail = {
                'status': api.code_not_enough_balance,
                'message': 'Your account does not have enough balance'
            }
            return jsonify(response_fail), 409

        biller = Biller.query.filter_by(biller_id=biller_id).first()
        
        # deduct billing amount from customer account
        customer_account.balance = customer_account.balance - billing_amount

        # expand biller balance
        biller.account.balance = biller.account.balance + billing_amount

        billing_transaction = Billing(
            billing_amount=billing_amount, 
            account_id=customer_account_id, 
            biller_id=biller_id, 
            billing_account_number=billing_account_number,
            remark=remark
        )

        db.session.add(billing_transaction)
        db.session.commit()

        response_success = {
            'status': api.code_success,
            'message': f'You have pay USD{billing_amount} to {biller.biller_name} on {billing_transaction.billing_date}'
        }

        return jsonify(response_success), 200
    except Exception as error:
        db.session.rollback()
        print(error)
        return 'server error', 500

@billing_router.route('/billing/update', methods=['PUT'])
@expects_json(schema=update_billing_schema)
def update_billing():
    return '', 501

@billing_router.route('/billing/delete', methods=['DELETE'])
def delete_billing():
    return '', 501
