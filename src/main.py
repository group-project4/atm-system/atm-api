from flask import Flask, g
from routes.biller_type_route import biller_type_router
from routes.biller_route import biller_router
from routes.user_route import user_router
from routes.billing_route import billing_router
from routes.transfer_route import transfer_router
from routes.deposit_route import deposit_router
from routes.withdrawal_route import withdraw_router
from routes.phone_topup_route import phone_topup_router
from routes.file_upload_route import file_upload_route
from routes.auth_route import auth_router
from var.database import connection_string
from flask_sqlalchemy import SQLAlchemy
from database.db import db
from flask_cors import CORS
from os.path import join, realpath
from flask import request
from jwt import encode, decode, DecodeError
from var import api

app = Flask(__name__)

#enable cors
CORS(app)

#config database
app.config['SQLALCHEMY_DATABASE_URI'] = connection_string

#init db
db.init_app(app)

#register route
app.register_blueprint(biller_type_router)
app.register_blueprint(biller_router)
app.register_blueprint(user_router)
app.register_blueprint(billing_router)
app.register_blueprint(transfer_router)
app.register_blueprint(deposit_router)
app.register_blueprint(withdraw_router)
app.register_blueprint(phone_topup_router)
app.register_blueprint(auth_router)
app.register_blueprint(file_upload_route)

@app.before_request
def checkAuth():
    """
    Check auth token
    """
    try:
        g.user = {}
        authorizationHeader = request.headers.get('Authorization')

        if authorizationHeader is not None and authorizationHeader.startswith('Bearer '):
            token = authorizationHeader[len('Bearer '): len(authorizationHeader)]
            payload = decode(token, api.secret_key, algorithms=['HS256'])
            g.user['user_id'] = payload.get('user_id')
            g.user['account_id'] = payload.get('account_id')
    except DecodeError as error_decode: 
        print(error_decode)
    except Exception as error:
        print(error)
        

if __name__ == '__main__':
    app.config['JSON_SORT_KEYS'] = False
    app.run(host='localhost', port=8000, debug=True)