from jwt import encode
from var import api

def generate_token(payload: dict, **kwargs) -> str:
    token = encode(payload, api.secret_key, 'HS256')
    return token
    