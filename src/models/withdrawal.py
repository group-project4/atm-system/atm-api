from sqlalchemy import Column, String, Integer, BigInteger, TIMESTAMP, Text, ForeignKey, text
from database.db import db

class Withdrawal(db.Model):
    __tablename__ = 'tb_withdrawal'
    withdraw_id = Column(Integer, primary_key=True)
    withdraw_amount = Column(BigInteger)
    withdraw_date = Column(TIMESTAMP, server_default=text('now()'))
    account_id = Column(Integer, ForeignKey('tb_account.account_id'))

    def as_json(self) -> dict: 
        return {
            'withdraw_id': self.withdraw_id,
            'withdraw_amount': self.withdraw_amount,
            'withdraw_date': self.withdraw_date,
            'account': self.account.as_json()
        }