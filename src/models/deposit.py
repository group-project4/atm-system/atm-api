from sqlalchemy import Column, String, Integer, BigInteger, TIMESTAMP, Text, ForeignKey, text
from database.db import db

class Deposit(db.Model):
    __tablename__ = 'tb_deposit'
    deposit_id = Column(Integer, primary_key=True)
    deposit_amount = Column(BigInteger)
    deposit_date = Column(TIMESTAMP, server_default=text('now()'))
    account_id = Column(Integer, ForeignKey('tb_account.account_id'))

    def as_json(self) -> dict: 
        return {
            'deposit_id': self.deposit_id,
            'deposit_amount': self.deposit_amount,
            'deposit_date': self.deposit_date,
            'account': self.account.as_json()
        }