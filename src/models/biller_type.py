from database.db import db
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

class BillerType(db.Model):
    __tablename__ = 'tb_biller_type'
    biller_type_id = Column(Integer, primary_key=True)
    biller_type_name = Column(String(100))
    billers = relationship('Biller', backref='biller_type')

    def as_json(self):
        biller_count = 0

        if self.billers is not None:
            biller_count = len(self.billers)

        return {
            'biller_type_id': self.biller_type_id,
            'biller_type_name': self.biller_type_name,
            'total_biller': biller_count
        }