from database.db import db
from sqlalchemy import Column, Integer, String, Text, TIMESTAMP, text, ForeignKey, Boolean
from sqlalchemy.orm import relationship

class User(db.Model):
    __tablename__ = 'tb_user'
    user_id = Column(Integer, primary_key=True)
    user_firstname = Column(String(100))
    user_lastname = Column(String(100))
    user_username = Column(String(100))
    user_password = Column(Text)
    user_gender = Column(String(10))
    user_dob = Column(TIMESTAMP)
    user_phone = Column(String(3))
    user_email = Column(String(100))
    user_address = Column(String(255))
    user_photo = Column(Text)
    user_role = Column(String(10))
    is_active = Column(Boolean, server_default=text('true'))
    account_id = Column(Integer, ForeignKey('tb_account.account_id'))
    created_date = Column(TIMESTAMP, server_default=text('now()'))

    def as_json(self):

        json_format = {
            'user_id': self.user_id,
            'user_firstname': self.user_firstname,
            'user_lastname': self.user_lastname,
            'user_username': self.user_username,
            'user_password': self.user_password,
            'user_gender': self.user_gender,
            'user_dob': self.user_dob,
            'user_phone': self.user_phone,
            'user_email': self.user_email,
            'user_address': self.user_address,
            'user_photo': self.user_photo,
            'user_role': self.user_role,
            'is_active': self.is_active,
            'created_date': self.created_date,
        }

        if self.account is not None:
            json_format['account'] = self.account.as_json()

        return json_format
