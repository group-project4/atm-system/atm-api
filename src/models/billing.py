from sqlalchemy import Column, String, Integer, BigInteger, TIMESTAMP, Text, ForeignKey, text
from database.db import db

class Billing(db.Model):
    __tablename__ = 'tb_billing'
    billing_id = Column(Integer, primary_key=True)
    billing_amount = Column(BigInteger)
    billing_account_number = Column(Text)
    billing_date = Column(TIMESTAMP, server_default=text('now()'))
    remark = Column(Text)
    account_id = Column(Integer, ForeignKey('tb_account.account_id'))
    biller_id = Column(Integer, ForeignKey('tb_biller.biller_id'))

    def as_json(self) -> dict: 
        return {
            'billing_id': self.biller_id,
            'billing_amount': self.billing_amount,
            'billing_account_number': self.billing_account_number,
            'billing_date': self.billing_date,
            'remark': self.remark,
            'biller': self.biller.as_json(),
            'customer': self.customer_account.as_json()
        }