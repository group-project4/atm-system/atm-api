from sqlalchemy import Column, String, Integer, BigInteger, TIMESTAMP, Text, ForeignKey, text
from database.db import db

class Transfer(db.Model):
    __tablename__ = 'tb_transfer'
    transfer_id = Column(Integer, primary_key=True)
    transfer_from_account = Column(Integer, ForeignKey('tb_account.account_id'))
    transfer_to_account = Column(Integer, ForeignKey('tb_account.account_id'))
    transfer_amount = Column(BigInteger)
    transfer_date = Column(TIMESTAMP, server_default=text('now()'))
    transfer_remark = Column(Text)

    def as_json(self) -> dict: 
        return {
            'transfer_id': self.transfer_id,
            'transfer_from_account': self.account_from.as_json(),
            'transfer_to_account': self.account_to.as_json(),
            'transfer_amount': self.transfer_amount,
            'transfer_date': self.transfer_date,
            'transfer_remark': self.transfer_remark
        }