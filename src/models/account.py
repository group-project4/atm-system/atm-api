from database.db import db
from sqlalchemy import Column, Integer, String, Boolean, BigInteger, TIMESTAMP, text, ForeignKey
from sqlalchemy.orm import relationship

class Account(db.Model):
    __tablename__ = 'tb_account'
    account_id = Column(Integer, primary_key=True)
    account_name = Column(String(100))
    is_active = Column(Boolean, server_default=text('true'))
    balance = Column(BigInteger, server_default=text('0'))
    account_type = Column(String(20))
    last_transaction_date = Column(TIMESTAMP)
    created_date = Column(TIMESTAMP, server_default=text('now()'))
    #mapp to object
    account_owner = relationship('User', backref='account')
    account_biller = relationship('Biller', backref='account')
    account_billing = relationship('Billing', backref='customer_account')
    account_deposit = relationship('Deposit', backref='account')
    account_withdraw = relationship('Withdrawal', backref='account')
    account_transfer_from = relationship('Transfer', backref='account_from', foreign_keys='Transfer.transfer_from_account')
    account_transfer_to = relationship('Transfer', backref='account_to', foreign_keys='Transfer.transfer_to_account')
    account_phone_topup = relationship('PhoneTopup', backref='account')

    def as_json(self):
        return {
            'account_id': self.account_id,
            'account_name': self.account_name,
            'is_active': self.is_active,
            'balance': self.balance,
            'account_type': self.account_type,
            'last_transaction_date': self.last_transaction_date,
            'created_date': self.created_date
        }