from sqlalchemy import Column, String, Integer, BigInteger, TIMESTAMP, Text, ForeignKey, text
from database.db import db

class PhoneTopup(db.Model):
    __tablename__ = 'tb_phone_topup'
    topup_id = Column(Integer, primary_key=True)
    topup_amount = Column(BigInteger)
    phone_number = Column(String(15))
    topup_date = Column(TIMESTAMP, server_default=text('now()'))
    topup_remark = Column(Text)
    account_id = Column(Integer, ForeignKey('tb_account.account_id'))
    biller_id = Column(Integer, ForeignKey('tb_biller.biller_id'))

    def as_json(self) -> dict: 
        return {
            'topup_id': self.topup_id,
            'topup_amount': self.topup_amount,
            'phone_number': self.phone_number,
            'topup_date': self.topup_date,
            'topup_remark': self.topup_remark,
            'account': self.account.as_json(),
            'biller': self.biller.as_json()
        }