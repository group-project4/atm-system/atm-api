from database.db import db
from sqlalchemy import Column, Integer, String, Text, TIMESTAMP,text, ForeignKey
from sqlalchemy.orm import relationship

class Biller(db.Model):
    __tablename__ = 'tb_biller'
    biller_id = Column(Integer, primary_key=True)
    biller_name = Column(String(100))
    biller_location = Column(Text)
    biller_phone = Column(String(50))
    biller_email = Column(String(255))
    biller_remark = Column(Text)
    biller_logo = Column(Text)
    biller_type_id = Column(Integer, ForeignKey('tb_biller_type.biller_type_id'))
    biller_account_id = Column(Integer, ForeignKey('tb_account.account_id'))
    created_date = Column(TIMESTAMP, server_default=text('now()'))
    #
    billing_transactions = relationship('Billing', backref='biller')
    biller_phone_topup = relationship('PhoneTopup', backref='biller')

    def as_json(self):
        biller_account = None
        if self.account is not None:
            biller_account = self.account.as_json()

        return {
            'biller_id': self.biller_id,
            'biller_name': self.biller_name,
            'biller_location': self.biller_location,
            'biller_phone': self.biller_phone,
            'biller_email': self.biller_email,
            'biller_remark': self.biller_remark,
            'biller_logo': self.biller_logo,
            'biller_type': self.biller_type.as_json(),
            'account': biller_account,
            'created_date': self.created_date
        }
