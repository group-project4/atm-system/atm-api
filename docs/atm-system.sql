DROP SCHEMA IF EXISTS public CASCADE;
CREATE SCHEMA  public;

CREATE TABLE  tb_account(
	account_id SERIAL, 
	account_name VARCHAR(100) NOT NULL, 
	is_active BOOLEAN NOT NULL DEFAULT true, 
	balance BIGINT NOT NULL DEFAULT 0,
	last_transaction_date TIMESTAMP,
	account_type VARCHAR (20) NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT NOW(),
	PRIMARY KEY (account_id)
);

CREATE TABLE tb_user(
	user_id SERIAL, 
	user_lastname VARCHAR(100) NOT NULL,
	user_firstname VARCHAR(100) NOT NULL,
	user_username VARCHAR(100) NOT NULL UNIQUE,
	user_password TEXT NOT NULL,
	user_gender VARCHAR(10) NOT NULL,
	user_dob TIMESTAMP NOT NULL, 
	user_phone VARCHAR(30) NOT NULL, 
	user_email VARCHAR(100) NOT NULL, 
	user_address VARCHAR(255) NOT NULL, 
	user_photo TEXT NOT NULL, 
	user_role VARCHAR(10) NOT NULL,
	is_active BOOLEAN NOT NULL DEFAULT true,
	account_id INTEGER,
	created_date TIMESTAMP NOT NULL DEFAULT NOW(), 
	PRIMARY KEY (user_id),
	FOREIGN KEY (account_id) REFERENCES tb_account(account_id)
);	


CREATE TABLE  tb_biller_type(
	biller_type_id SERIAL, 
	biller_type_name VARCHAR (100) NOT NULL,
	PRIMARY KEY (biller_type_id)
);


CREATE TABLE  tb_biller(
	biller_id SERIAL, 
	biller_name VARCHAR (100) NOT NULL,
	biller_location TEXT NOT NULL, 
	biller_phone VARCHAR(30) NOT NULL, 
	biller_email VARCHAR (255) NOT NULL, 
	biller_remark TEXT, 
	biller_logo TEXT NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT NOW(), 
	biller_type_id INTEGER NOT NULL,
	biller_account_id INTEGER,
	PRIMARY KEY (biller_id),
	FOREIGN KEY (biller_type_id) REFERENCES tb_biller_type(biller_type_id),
	FOREIGN KEY (biller_account_id) REFERENCES tb_account(account_id)
);


CREATE TABLE  tb_billing(
	billing_id SERIAL,
	billing_amount BIGINT NOT NULL,
	billing_account_number TEXT,
	billing_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	remark TEXT,
	account_id INTEGER NOT NULL,
	biller_id INTEGER NOT NULL,
	PRIMARY KEY (billing_id),
	FOREIGN KEY (account_id) REFERENCES tb_account(account_id),
	FOREIGN KEY (biller_id) REFERENCES tb_biller (biller_id)
);


CREATE TABLE  tb_deposit(
	deposit_id SERIAL,
	deposit_amount BIGINT NOT NULL,
	deposit_date TIMESTAMPTZ DEFAULT NOW(),
	account_id INTEGER NOT NULL,
	PRIMARY KEY (deposit_id),
	FOREIGN KEY (account_id) REFERENCES tb_account(account_id)
);


CREATE TABLE  tb_withdrawal(
	withdraw_id SERIAL,
	withdraw_amount BIGINT NOT NULL,
	withdraw_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	account_id INTEGER NOT NULL,
	PRIMARY KEY (withdraw_id),
	FOREIGN KEY (account_id) REFERENCES tb_account(account_id)
);


CREATE TABLE  tb_transfer(
	transfer_id SERIAL,
	transfer_from_account INTEGER NOT NULL,
	transfer_to_account INTEGER NOT NULL,
	transfer_amount BIGINT NOT NULL,
	transfer_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	transfer_remark TEXT,
	PRIMARY KEY (transfer_id),
	FOREIGN KEY (transfer_from_account) REFERENCES tb_account(account_id),
	FOREIGN KEY (transfer_to_account) REFERENCES tb_account(account_id)
);


CREATE TABLE  tb_phone_topup(
	topup_id SERIAL,
	topup_amount BIGINT NOT NULL,
	phone_number VARCHAR(15) NOT NULL,
	topup_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	topup_remark TEXT,
	account_id INTEGER NOT NULL,
	biller_id INTEGER NOT NULL,
	PRIMARY KEY (topup_id),
	FOREIGN KEY (account_id) REFERENCES tb_account(account_id),
	FOREIGN KEY (biller_id) REFERENCES tb_biller(biller_id)
);

INSERT INTO tb_biller_type (biller_type_name) VALUES ('ISP');
INSERT INTO tb_biller_type (biller_type_name) VALUES ('Bank');
INSERT INTO tb_biller_type (biller_type_name) VALUES ('Electricity');
INSERT INTO tb_biller_type (biller_type_name) VALUES ('Water Supply');
INSERT INTO tb_biller_type (biller_type_name) VALUES ('Telecome');

INSERT INTO tb_user 
(
	user_lastname, 
	user_firstname, 
	user_username, 
	user_password, 
	user_gender, 
	user_dob, 
	user_phone, 
	user_email, 
	user_address, 
	user_photo, 
	user_role
) 
VALUES 
(
	'System',
	'Admin',
	'root',
	'root',
	'male',
	'now()',
	'012343384',
	'root@gmail.com',
	'',
	'',
	'admin'
);